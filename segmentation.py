import cv2 as cv
# from matplotlib import pyplot as plt
import numpy as np
import math
# from scipy import ndimage
from statistics import mean, StatisticsError


class Segmentation:
	m_j_plus_1 = None
	m_j = None
	m_0 = None
	path = []
	
	def __init__(self):
		self.original_image = self.read_image()
	
	def mean_shift(self):
		h_s = int(input("Please Input the bandwidth for space:"))
		h_r = int(input("Please Input the bandwidth for grey scale range:"))
		
		# ----------------------------->
		img_after_mean_shift = np.pad(self.original_image, ((h_s, h_s), (h_s, h_s), (0, 0)), 'constant', constant_values=0)
		# ----------------------------->
		
		image_height = len(img_after_mean_shift)
		image_width = len(img_after_mean_shift[0])
		
		# initial x^i_s, x^i_r
		x = h_s
		while x in range(h_s, image_height-h_s):
			y = h_s
			while y in range(h_s, image_width-h_s):
				# self.path = []
				self.m_0 = [x, y, img_after_mean_shift[x][y]]
				self.m_j = self.m_0
				# self.path.append(self.m_j)
				
				try:
					self.m_j_plus_1 = self.get_new_mean(x, y, self.m_j, img_after_mean_shift, h_s, h_r)
					# self.path.append(self.m_j_plus_1)
				except StatisticsError:
					continue
					
				stop_or_not = self.verify_mean_shift_stop_or_not(x, y, self.m_j_plus_1, self.m_j, img_after_mean_shift, h_s, h_r)
				i = 0
				while stop_or_not == 0:
					self.m_j = self.m_j_plus_1
					
					try:
						self.m_j_plus_1 = self.get_new_mean(self.m_j[0], self.m_j[1], self.m_j, img_after_mean_shift, h_s, h_r)
						# self.path.append(self.m_j_plus_1)
					except StatisticsError:
						break
					
					if i >= 10:
						img_after_mean_shift[x][y] = self.m_j_plus_1[2]
						# img_after_mean_shift[self.m_j_plus_1[0] - int(0.5*h_s):self.m_j_plus_1[0] + int(0.5*h_s) + 1, self.m_j_plus_1[1] - int(0.5*h_s):self.m_j_plus_1[1] + int(0.5*h_s) + 1] = self.m_j_plus_1[2]
						
						# for j in self.path:
						# 	img_after_mean_shift[j[0]-int(h_s):j[0]+int(h_s)+1, j[1]-int(h_s):j[1]+int(h_s)+1] = self.m_j_plus_1[2]
						break
					stop_or_not = self.verify_mean_shift_stop_or_not(x, y, self.m_j_plus_1, self.m_j, img_after_mean_shift, h_s, h_r)
					i = i + 1
				# if y == image_width - h_s - 1:
				# 	break
				# y = y + 2 * h_s + 1
				y = y + 1
			# 	if y > image_width - h_s - 1:
			# 		y = image_width - h_s - 1
			# if x == image_height - h_s - 1:
			# 	break
			# x = x + 2 * h_s + 1
			x = x + 1
			# if x > image_height - h_s - 1:
			# 	x = image_height - h_s - 1
		# ----------------------------->
		
		self.original_image = img_after_mean_shift[h_s:image_height-h_s, h_s:image_width-h_s]
		# ----------------------------->
	
	def get_new_mean(self, x, y, m_0, img_after_mean_shift, h_s, h_r):
		mask_size = 2 * h_s + 1
		
		m_j_plus_one_0 = []
		m_j_plus_one_1 = []
		m_j_plus_one_2 = []
		m_j_plus_one_3 = []
		m_j_plus_one_4 = []
		
		for f_x in range(mask_size):
			for f_y in range(mask_size):
				pixel = img_after_mean_shift[(x + f_x - h_s)][(y + f_y - h_s)]
				# window box
				if (pixel[0] >= (m_0[2][0] - h_r)) and (pixel[0] <= (m_0[2][0] + h_r)) and (pixel[1] >= (m_0[2][1] - h_r)) and (pixel[1] <= (m_0[2][1] + h_r)) and (pixel[2] >= (m_0[2][2] - h_r)) and (pixel[2] <= (m_0[2][2] + h_r)):
					m_j_plus_one_0.append(x)
					m_j_plus_one_1.append(y)
					m_j_plus_one_2.append(float(pixel[0]))
					m_j_plus_one_3.append(float(pixel[1]))
					m_j_plus_one_4.append(float(pixel[2]))
		m_j = [mean(m_j_plus_one_0), mean(m_j_plus_one_1), np.array([mean(m_j_plus_one_2), mean(m_j_plus_one_3), mean(m_j_plus_one_4)])]
		return m_j
	
	def verify_mean_shift_stop_or_not(self, x, y, m_j, m_0, img_after_mean_shift, h_s, h_r):
		
		if (abs(m_j[0] - m_0[0]) < h_s) and (abs(m_j[1] - m_0[1]) < h_s) and (
				abs(m_j[2][0] - m_0[2][0]) < h_r) and (abs(m_j[2][1] - m_0[2][1]) < h_r)\
				and (abs(m_j[2][2] - m_0[2][2]) < h_r):
			img_after_mean_shift[x][y] = m_j[2]
			# img_after_mean_shift[m_j[0] - int(0.5*h_s):m_j[0] + int(0.5*h_s) + 1, m_j[1] - int(0.5*h_s):m_j[1] + int(0.5*h_s) + 1] = m_j[2]
			
			# for j in self.path:
			# 	img_after_mean_shift[j[0] - int(h_s):j[0] + int(h_s) + 1, j[1] - int(h_s):j[1] + int(h_s) + 1] = m_j[2]
			return 1
		return 0
	
	def read_image(self):
		# img = np.array([[[1, 1, 1], [1, 1, 1]],
		#                 [[1, 1, 1], [1, 1, 1]]])
		
		# path = input("Please input the path of image:")
		path = '/home/qyc/PycharmProjects/cs463_final/image/image3.ppm'
		img = cv.imread(path)
		# first 100 rows, 300 column
		# return img[0:100, 0:300]
		return img
		
	# def split_into_rgb_channels(self):
	# 	red = self.original_image[:, :, 2]
	# 	green = self.original_image[:, :, 1]
	# 	blue = self.original_image[:, :, 0]
	# 	return red, green, blue
	
	def get_enhanced_image(self):
		return self.original_image

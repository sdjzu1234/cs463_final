from segmentation import *
from multiprocessing import Pool
import numpy as np
from itertools import *
import time

# segmentation
seg = Segmentation()

img = seg.get_enhanced_image()
cv.imwrite('3.ppm', img)

h_s = 3
h_r = 30
epsilon = 0.1

# ----------------------------->
img_after_mean_shift = np.pad(img, ((h_s, h_s), (h_s, h_s), (0, 0)), 'constant', constant_values=0)

# ----------------------------->

image_height = len(img_after_mean_shift)
image_width = len(img_after_mean_shift[0])

arg_pairs = [(x, y) for x in range(h_s, image_height - h_s) for y in range(h_s, image_width - h_s)]


def mean_shift(x, y):
	# x, y = pair
	# self.path = []
	m_0 = [x, y, img_after_mean_shift[x][y][0], img_after_mean_shift[x][y][1], img_after_mean_shift[x][y][2]]
	m_j = m_0
	# path.append(m_j)
	
	m_j_plus_1 = get_new_mean(x, y, m_j, img_after_mean_shift, h_s, h_r)
	
	stop_or_not = verify_mean_shift_stop_or_not(x, y, m_j_plus_1, m_j, img_after_mean_shift, epsilon * h_s, epsilon * h_r)
	# i = 0
	while stop_or_not == 0:
		m_j = m_j_plus_1
		
		try:
			m_j_plus_1 = get_new_mean(m_j[0], m_j[1], m_j, img_after_mean_shift, h_s, h_r)
		# path.append(m_j_plus_1)
		except StatisticsError:
			break
		
		# if i >= 10:
		# 	img_after_mean_shift[x][y] = m_j_plus_1[2]
		# 	return m_j_plus_1
			# img_after_mean_shift[m_j_plus_1[0] - int(0.5*h_s):m_j_plus_1[0] + int(0.5*h_s) + 1, m_j_plus_1[1] - int(0.5*h_s):m_j_plus_1[1] + int(0.5*h_s) + 1] = m_j_plus_1[2]
			# for j in path:
			# 	img_after_mean_shift[j[0]-int(h_s):j[0]+int(h_s)+1, j[1]-int(h_s):j[1]+int(h_s)+1] = m_j_plus_1[2]

		stop_or_not = verify_mean_shift_stop_or_not(x, y, m_j_plus_1, m_j, img_after_mean_shift, epsilon * h_s, epsilon * h_r)
		# i = i + 1
	return m_j_plus_1
	
	
def get_new_mean(x, y, m_0, img_after_mean_shift, h_s, h_r):
	mask_size = 2 * h_s + 1
	
	m_j_plus_one_0 = []
	m_j_plus_one_1 = []
	m_j_plus_one_2 = []
	m_j_plus_one_3 = []
	m_j_plus_one_4 = []
	
	for f_x in range(mask_size):
		for f_y in range(mask_size):
			pixel = img_after_mean_shift[(x + f_x - h_s)][(y + f_y - h_s)]
			# window box
			if (pixel[0] >= (m_0[2] - h_r)) and (pixel[0] <= (m_0[2] + h_r)) and (
					pixel[1] >= (m_0[3] - h_r)) and (pixel[1] <= (m_0[3] + h_r)) and (
					pixel[2] >= (m_0[4] - h_r)) and (pixel[2] <= (m_0[4] + h_r)):
				m_j_plus_one_0.append(x)
				m_j_plus_one_1.append(y)
				m_j_plus_one_2.append(float(pixel[0]))
				m_j_plus_one_3.append(float(pixel[1]))
				m_j_plus_one_4.append(float(pixel[2]))
	m_j = [mean(m_j_plus_one_0), mean(m_j_plus_one_1), mean(m_j_plus_one_2), mean(m_j_plus_one_3), mean(m_j_plus_one_4)]
	return m_j


def verify_mean_shift_stop_or_not(x, y, m_j, m_0, img_after_mean_shift, h_s, h_r):
	if (abs(m_j[0] - m_0[0]) < h_s) and (abs(m_j[1] - m_0[1]) < h_s) and (
			abs(m_j[2] - m_0[2]) < h_r) and (abs(m_j[3] - m_0[3]) < h_r) \
			and (abs(m_j[4] - m_0[4]) < h_r):
		# img_after_mean_shift[x][y] = m_j[2]
		# img_after_mean_shift[m_j[0] - int(0.5*h_s):m_j[0] + int(0.5*h_s) + 1, m_j[1] - int(0.5*h_s):m_j[1] + int(0.5*h_s) + 1] = m_j[2]
		
		# for j in self.path:
		# 	img_after_mean_shift[j[0] - int(h_s):j[0] + int(h_s) + 1, j[1] - int(h_s):j[1] + int(h_s) + 1] = m_j[2]
		return 1
	return 0


def smooth():
	# Create a pool of worker processes, each able to use a CPU core
	pool = Pool()
	print('\n---' + 'hs= ' + str(h_s) + ', hr= ' + str(h_r) + ', epsilon= ' + str(epsilon) + '---\n')
	
	# Prepare the arguments, one per function invocation (tuples to fake multiple)
	# start_time = time.time()
	# arg_pairs = [(x, y) for x in range(h_s, image_height - h_s) for y in range(h_s, image_width - h_s)]
	# end_time = (time.time() - start_time)
	# print("\n--- Pair x, y: %s seconds ---\n" % end_time)
	
	start_time = time.time()
	result = pool.starmap(mean_shift, arg_pairs)
	end_time = (time.time() - start_time) / 60
	print("\n--- Mean shift: %s mins ---\n" % end_time)
	
	start_time = time.time()
	smoothed_img_with_location = np.array(result).reshape(((image_height - 2 * h_s), (image_width - 2 * h_s), 5))
	end_time = (time.time() - start_time)
	print("\n--- Reshape to matrix: %s seconds ---\n" % end_time)
	
	# location = smoothed_img_with_location[:, :, 0:2]
	np.save('3_hs' + str(h_s) + '_hr' + str(h_r) + '_epsilon' + str(epsilon) + '.npy', smoothed_img_with_location)
	

def seg():
	smoothed_img_with_location = np.load('3_hs' + str(h_s) + '_hr' + str(h_r) + '_epsilon' + str(epsilon) + '.npy')
	smoothed_img = smoothed_img_with_location[:, :, 2:5]
	
	from copy import deepcopy
	start_time = time.time()
	img_after_mean_shift[h_s:image_height - h_s, h_s:image_width - h_s] = deepcopy(smoothed_img)
	end_time = (time.time() - start_time)
	print("\n---copy: %s seconds ---\n" % end_time)

	cv.imwrite('3_hs' + str(h_s) + '_hr' + str(h_r) + '_epsilon' + str(epsilon) + '.ppm', img_after_mean_shift[h_s:image_height - h_s, h_s:image_width - h_s])
	

if __name__ == '__main__':
	smooth()
	seg()

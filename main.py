"""
	description:
		author: Yanchun Qi
		date: 30/03/2019
"""
from gray_scale_filtering import *
from segmentation import *


def main():
	# # smoothing
	# gray_scale_filter = Enhancement()
	# gray_scale_filter.mean_shift()
	# img = gray_scale_filter.get_enhanced_image()
	# cv.imwrite('1_result.pgm', img)
	
	# segmentation
	seg = Segmentation()
	
	img = seg.get_enhanced_image()
	cv.imwrite('4.ppm', img)
	
	seg.mean_shift()

	img = seg.get_enhanced_image()
	cv.imwrite('4_hs10_hr50.ppm', img)


if __name__ == "__main__":  # execute the main function
	main()

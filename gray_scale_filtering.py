import cv2 as cv
from matplotlib import pyplot as plt
import numpy as np
import math
from scipy import ndimage
from statistics import mean, StatisticsError


class Enhancement:
	original_image = None
	new_m_j_plus_one = None
	m_j_plus_one = None
	
	def __init__(self):
		self.original_image = self.read_image()
	
	def mean_shift(self):
		h_s = int(input("Please Input the bandwidth for space:"))
		h_r = int(input("Please Input the bandwidth for grey scale range:"))
		
		img_after_mean_shift = self.original_image
		
		image_height = len(img_after_mean_shift)
		image_width = len(img_after_mean_shift[0])
		
		three_d_image = np.empty_like(self.original_image, dtype=object)
		
		# initial x^i_s, x^i_r
		for x in range(image_height):
			for y in range(image_width):
				three_d_image[x][y] = [x, y, self.original_image[x][y]]
		
		for x in range(image_height):
			for y in range(image_width):
				m_0 = three_d_image[x][y]
				self.m_j_plus_one = m_0
				try:
					window, self.new_m_j_plus_one = self.get_window(x, y, self.m_j_plus_one, three_d_image, image_height, image_width, h_s, h_r)
				except StatisticsError:
					continue
			
				stop_or_not = self.verify_mean_shift_stop_or_not(x, y, self.new_m_j_plus_one, self.m_j_plus_one, img_after_mean_shift, h_s, h_r, three_d_image, image_height, image_width)
				i = 0
				while stop_or_not == 0:
					self.m_j_plus_one = self.new_m_j_plus_one
					try:
						new_window, self.new_m_j_plus_one = self.get_window(self.m_j_plus_one[0], self.m_j_plus_one[1], self.m_j_plus_one, three_d_image, image_height, image_width, h_s, h_r)
					except StatisticsError:
						break
					if i >= 5:
						break
					stop_or_not = self.verify_mean_shift_stop_or_not(x, y, self.new_m_j_plus_one, self.m_j_plus_one, img_after_mean_shift, h_s, h_r, three_d_image, image_height, image_width)
					i = i + 1
		self.original_image = img_after_mean_shift
				
	def get_window(self, x, y, m_0, three_d_image, image_height, image_width, h_s, h_r):
		mask_size = 2 * h_s + 1
		
		window = []
		m_j_plus_one_0 = []
		m_j_plus_one_1 = []
		m_j_plus_one_2 = []
		
		for f_x in range(mask_size):
			for f_y in range(mask_size):
				pixel = three_d_image[(x + f_x - h_s) % image_height][(y + f_y - h_s) % image_width]
				if (pixel[2] >= (m_0[2] - h_r)) and (pixel[2] <= (m_0[2] + h_r)):
					m_j_plus_one_0.append(pixel[0])
					m_j_plus_one_1.append(pixel[1])
					m_j_plus_one_2.append(float(pixel[2]))
					window.append(pixel)
		m_j_plus_one = [round(mean(m_j_plus_one_0)), round(mean(m_j_plus_one_1)), mean(m_j_plus_one_2)]
		return window, m_j_plus_one
		
	def verify_mean_shift_stop_or_not(self, x, y, m_j_plus_one, m_0, img_after_mean_shift, h_s, h_r, three_d_image, image_height, image_width):
		
		if (abs(m_j_plus_one[0] - m_0[0]) < h_s) and (abs(m_j_plus_one[1] - m_0[1]) < h_s) and (abs(m_j_plus_one[2] - m_0[2]) < h_r):
			img_after_mean_shift[x][y] = m_j_plus_one[2]
			return 1
		return 0
			
	def read_image(self):
		# path = input("Please input the path of image:")
		path = 'image1.pgm'
		img = cv.imread(path, -1)
		# img = np.array([[0,0,0,0,0,0],
		#                 [0,17,17,17,0,0],
		#                 [0,15,15,15,0,0],
		#                 [0,10,10,10,0,0],
		#                 [0,45,45,45,0,0],
		#                 [0,0,0,0,0,0]
		# ])
		return img
	
	def get_enhanced_image(self):
		return self.original_image

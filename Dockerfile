FROM python:latest

RUN apt-get update && apt-get install -y

RUN pip install opencv-python

# ADD ./multithread.py segmentation.py image4.ppm /root/

WORKDIR /root

ENTRYPOINT python3 multithread.py
